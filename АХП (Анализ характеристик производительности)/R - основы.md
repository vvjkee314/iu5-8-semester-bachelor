#R #АХП 

совсем база здесь: [[Как сдавать ЛР]]
Некоторые полезные функции в методе ЛР-1 [[Laboratory_work.ipynb]]

Еще гайд: https://agricolamz.github.io/2020-2021-ds4dh/intro.html
На YouTube: https://www.youtube.com/watch?v=6HdhXZIPZec
На Stepik (то же, что и на YouTube, но с практикой): https://stepik.org/course/497/syllabus

`<<-` оператор глобального присваивания

`ls()` - список переменных в глобальном окружении
`rm(list = ls())` - удаление переменных из глобального окружения. **Периодически нужно вызывать**, чтобы экономить ОЗУ.


## Векторы

векторизация - ключевая концепция языка R.
все скалярные значения в R - это вектора длины 1.
индексация с 1.
Создание вектора `vector(length=2)` + присвоить все элементы по индексу, `c(5, 8)`.

Вот так можно выделить память.
```r
x <- vector(mode='character', length=100)
x <-character(100)  # то же самое. про типы см ниже
```

Вектора можно вкладывать друг в друга. Тогда итоговый вектор получается плоским. 
Все элементы вектора - **одного типа**.
Если создать в-тор с разными типами, он последовательно попробует перобразовать все элементы к `logical - integer - double - character`.

`letters` - вектор, содержайщий 26 букв `a`-`z`.
```r
> letters
 [1] "a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z"
```


## Числовые посл-ти

5:9 - от 5 до 9 включительно с шагом 1 = 5, 6, 7, 8, 9
3:-1 - 3, 2, 1, 0, -1

сложные посл-ти
```r
> seq(1, 2, by=0.25)
[1] 1.00 1.25 1.50 1.75 2.00
> seq(1, 2, by=0.24)
[1] 1.00 1.24 1.48 1.72 1.96
```
можно автоматически вычислить шаг по кол-ву элементов
```r
> seq(3,4, length.out=5)
[1] 3.00 3.25 3.50 3.75 4.00
```
имена аргументов с точками можно сокращать:
```r
> seq(3,4, length=5)   # то же самое, что и length.out
[1] 3.00 3.25 3.50 3.75 4.00
```

повторение
```r
> rep(1:3, times=3)
[1] 1 2 3 1 2 3 1 2 3

> rep(1:3, each=3)
[1] 1 1 1 2 2 2 3 3 3

> rep(1:3, length.out=5)
[1] 1 2 3 1 2
```


## Функции

```r
HH1<-function(p){return(sum(c(-0.3, 0.006, 0.2884, 0.088, 0.132, -0.15)*p))
```
Если не прописать `return`, функция автоматически вернет результат последней операции. Можно создавать **анонимные** функции. Можно создавать короткие формы записи:
```r
function() sum(x^2)
```

Есть именнованные аргументы и значения по умолчанию.


## Сравнение double
Делать через
```r
> 1 + 0.05 == 1.15
[1] FALSE
> all.equal(1 + 0.05, 1.05)
[1] TRUE
```


## Сортировка
алгоритм Shellsort. По умолчанию - по возрастанию
```r
sort(вектор)
sort(x, decreasing = TRUE)  # по убыванию  
unique(вектор) - удаление повторяющихся элементов
```


## Типы данных
-   logical. TRUE, FALSE, T, F
-   numeric aka double
-   integer.
-   complex.
-   character. Строки
-   raw. Байтовые последовательности
-  NA, NaN, Null

Проверка типа - операторы `typeof(<переменная>)`, `is.<имя типа>`:
```r
> x <- "dataset"
> typeof(x)
[1] "character"

> attributes(x)
[1] NULL

> y <- 1:10
> y
[1]  1  2  3  4  5  6  7  8  9 10

> typeof(y)
[1] "integer"
length(y)
[1] 10

> z <- as.numeric(y)
> z
[1]  1  2  3  4  5  6  7  8  9 10
> typeof(z)
[1] "double"
```

### NA, NaN, Null
В R есть три похожих ключевых слова, NA, NaN и NULL. Они различаются по смысловой нагрузке. 
-   NA -- это пропущенное значение ("not available"). Например, респондент не ответил на все вопросы предложенной анкеты, или данные с метеостанции за определённый период потерялись из-за сбоя оборудования. NA в этом случае обозначает, что эти данные существуют и имеют смысл, но их не удалось узнать.
-   NaN -- "not-a-number" -- результат недопустимой арифметической операции, например 0/0 или Inf - Inf.
-   NULL -- отсутствие объекта, "пустота". Применяется в тех случаях, когда объект действительно не существует, не может иметь осмысленного значения.

Для проверки значений есть три функции, , соответственно.
Полезно:
```r
> is.na(NA)
[1] TRUE
> is.nan(NaN)
[1] TRUE
> is.null(NULL)
[1] TRUE
```

-   `class()` - what kind of object is it (high-level)?
-   `typeof()` - what is the object’s data type (low-level)?

`as.<имя типа>` - каст руками. Если не все элементы возможно привести к указанному типу, выводится warning, а на месте элемента появляется `NA`. `double-to-integer` каст удаляет дробную часть: 1/3 -> 0.
```r
> x <- c(TRUE, 1, 1/3, 0, 'f')
> as.logical(x)
[1] TRUE   NA   NA   NA   NA
> as.integer(x)
[1] NA  1  0  0 NA
Warning message:
NAs introduced by coercion 
> as.double(x)
[1]        NA 1.0000000 0.3333333 0.0000000        NA
Warning message:
NAs introduced by coercion 
> as.character(x)
[1] "TRUE"              "1"                 "0.333333333333333" "0"                 "f"  
```


## Длина
`length()`
можно изменять. Элементы либо удаляются, либо `NA`:
```r
> x <- 1:100
> length(x) <-4; x
[1] 1 2 3 4
> length(x) <-7; x
[1]  1  2  3  4 NA NA NA
```


## Именнованные векторы
2 способа получить: специальный синтаксис для `c` и `names`. С помощью `names` можно задать имена или получить список имен. Имена полей векторов - только строки. Имена можно удалить, присвоив `NULL`.

```r
> v = c("Mary", "Sue"); v
[1] "Mary" "Sue" 
> names(v) = c("First", "Last"); v
 First   Last 
"Mary"  "Sue" 
> v["First"]
 First 
"Mary" 
> v[c("Last", "First")] # reverse
  Last  First 
 "Sue" "Mary"
 
> v2 <- c(uno = 5, dos = 6, 's t r i n g' = 7, 8); names(v2)
[1] "uno"         "dos"         "s t r i n g" ""

> names(v2) <- NULL; v2
[1] 5 6 7 8
```


## Векторная арифметика

Арифметические операторы векторизованы - применяются поэлементно. В т.ч. оператор `&`.
Многие функции имеют векторизацию: `sqrt`, `floor`. Но не все - где это очевидно, например, `sum`.
```r
> sqrt(1:4)
[1] 1.000000 1.414214 1.732051 2.000000
> sum(1:4)
[1] 10

# степени 2-ки
> 2 ^ (0:10)
 [1]    1    2    4    8   16   32   64  128  256  512 1024
```

См. [[#all, any, which, match, reverse, identical]]

Можно вызывать свои функции на векторах
```r
HH1<-function(p){return(sum(c(-0.3, 0.006, 0.2884, 0.088, 0.132, -0.15)*p))}
```


## Соглашения по именам
https://stackoverflow.com/questions/9195718/variable-name-restrictions-in-r


## Управляющие констуркции
```r
if (x > 2) {

} else {  # else на одной строке с }

}

# скобки можно попустить
if (!p | !l) return(FALSE)
else return(TRUE)
```
Если передать вектор в `if`, то возмется только первое значение, и будет выведен warning. Для проверки условия на векторе для каждого элемента используется `ifelse`:
```r
> x <- runif(4)
> ifelse(x > 2/3, 'камень', ifelse(x > 1/3, 'ножницы', 'бумага'))
[1] "ножницы" "бумага"  "камень"  "камень"
```
Есть `switch`. Первый аргумент - строка, остальные - правила вывода по входной строке.


### Циклы
- repeat {} - выход по break
- while () {}
- for (i in 1:8) {} | for (i in letters) {}
- break и next
Не использовать циклы там, где можно применить векторизацию. Внутри куча оптимизаций по выделению памяти.


## Замер времени выполнения кода
```r
system.time({
	код
})
```


## Пакеты

Ядро языка - пакет base. Пакет - завершенный изолированный модуль. Библиотека - не пакет, а место на диске, где он хранится.
`.libPaths()` - выводит список путей в ФС, где лежат пакеты.
`installed.packages()` - список установленных.

`require(имя пакета)` - как `library()`, только если пакета нет, вернет не ошибку, а `FALSE`.

`sessionInfo()` - для дебага пакетов, если какой-то не загрузился. Пакеты могут обновляться или конфликтовать.

`help(package = "xts")` - справка по пакетам.


## Правила переписывания (recycling rule)

Как работает арифметика на векторах разной длины?
1. Длина результата равна длине большего из векторов
2. Меньший вектор дублируется (переписывается) несколько раз, чтобы длина переписанного вектора совпала с длиной большего
3. Если длина большего вектора не делится нацело на длину меньшего выдаётся предупреждение

Это нужно, чтобы производить арифметические и логические операции над векторами и числами.
Срабатывает и на оператор взятия по индексу.


## Индексы

Положительные - как всегда. Отрицательные - все, кроме.

```r
> x <- seq(10, 100, by=10)
> x[]
 [1]  10  20  30  40  50  60  70  80  90 100
> x[1]
[1] 10
> x[10]
[1] 100
> x[-1]
[1]  20  30  40  50  60  70  80  90 100
> x[-10]
[1] 10 20 30 40 50 60 70 80 90
> x[3:4]
[1] 30 40
```

Логические - маска

```r
> x[rep(c(TRUE, FALSE), 5)]
[1] 10 30 50 70 90
> x[c(TRUE, FALSE)]   # сработало переписывание
[1] 10 30 50 70 90
> x[x>71 & x<82]
[1] 80
```

Индексация по имени. См [[#Именнованные векторы]].


## all, any, which, match, reverse, identical

`all`, `any` - как в питоне. which возвращает вектор позиций элементов, удовлетворяющих условию. Как в `numpy`. Есть `which.min(x)` и `which.max(x)`.

Если есть повторения, то `which.min` вернет только один минимум, первый. Чтобы получить индексы всех минимумов, нужно делать по-другому:
```r
> x <- sample(1:6, 10, replace=T); x
 [1] 4 1 1 5 6 3 1 2 5 6
> which.min(x)
[1] 2
> which(x == min(x))
[1] 2 3 7
```

`match(x,y)` - возвращает индексы совпадающих элементов, и NA, если не совпало.
```r
> x<-c(1,2,3,4,5); y<-c(4,5,6,7,1)
> match(x,y)
5, <NA>, <NA>, 1 ,2
```

`identical` - сравнения векторов
`rev(x)` - reverse


## Аттрибуты объектов

-   `length()` - how long is it? What about two dimensional objects? Есть у всех объектов.
-   `attributes()` - does it have any metadata
Опциональные
- dim
- dimnames
- names

Аттрибуты могут иметь любые названия:

```r
> x <- 1:10
> dim(x)
NULL
> attr(x,"dim") <- c(2, 5)
> dim(x)
[1] 2 5
```


## Строки

paste - конкатенация строк
paste0 - конкатенация строк без пробелов

```r
W<-data.frame(P1,P2,P3,P4,P5,P6)
names<-c()
for (i in c(1:m)){ names<-c(names, paste("W",as.character(i)))}
```



## Хранение объектов в ФС

См. [[Как сдавать ЛР]]


## Матрицы

```r
matrix(<вектор> nrow=2, ncol=3)  # по умолчанию заполняется по столбцам. Один из аругментов nrow или ncol можно опустить
matrix(<вектор> nrow=2, ncol=3, byrow=TRUE) # заполнение по строкам

> matrix(1:6, ncol=3)
     [,1] [,2] [,3]
[1,]    1    3    5
[2,]    2    4    6
> > matrix(1:6, ncol=3, byrow=T)
     [,1] [,2] [,3]
[1,]    1    2    3
[2,]    4    5    6
```

Для заполнения матрицы вектором меньшей, чем нужно, длины применяются [[#Правила переписывания (recycling rule)]].

Матрица отличается от [[#Векторы]] только наличием атрибута **dim**.
```r
> dim(m)
[1] 2 3
> nrow(m)
[1] 2
> ncol(m)
[1] 3
```

Можно сделать из матрицы вектор. Обратно тоже можно: 

```r
dim(m) <- NULL
```

### Арифметические операции над матрицами

Действуют [[#Правила переписывания (recycling rule)]].
Сложение и умножение - поэлементное.
Оператор для умножения матриц из ЛинАла: `%*%`.

### Индексация

Как у векторов, только 2 размерности. Если один из аргументов опустить, получим всю строку/столбец.

`W[i,j]` - элемент i-ой строки, j-ого столбца
`W[i, ]` - вектор элементов i-ой строки
`W[, j]` - вектор элементов j-ого столбца
`W[i]` - число, индексация как в векторе
`W[v_i, v_j]` - подматрица v_i, v_j - векторы.
```r
base[2:(diam-1), 2:(diam-1)] <- 1 # скобки важны! : имеет более выоский приоритет
```

Обнулить строку:
```r
[1, ] <- 0
```

Отрицательные индексы имеют смысл кроме. Как у [[#Векторы]].

Есть логическая индексация:
```r
> m <-matrix(1:10, ncol=5); m
     [,1] [,2] [,3] [,4] [,5]
[1,]    1    3    5    7    9
[2,]    2    4    6    8   10
> m[m<=5] <-5; m
     [,1] [,2] [,3] [,4] [,5]
[1,]    5    5    5    7    9
[2,]    5    5    6    8   10
```

### Схлопывание размерностей

Индексация столбца/строки возвращает вектор. Но можно сохранить имена строк/столбцов, передав аргумент `drop=FALSE`. Обрати внимание на пустой второй аргумент в последнем вводе.
```r
> m <-matrix(1:10, ncol=5)
> ind <- c(1,3,5)
> m[,ind]
     [,1] [,2] [,3]            # матрица
[1,]    1    5    9
[2,]    2    6   10
> ind <- 3
> m[,ind]
[1] 5 6                        # вектор
> m[,ind, drop=FALSE]
     [,1]                      # матрица
[1,]    5
[2,]    6

> m[1,]
[1] 1 3 5 7 9                  # вектор
> m[1, , drop=FALSE]
     [,1] [,2] [,3] [,4] [,5]  # матрица
[1,]    1    3    5    7    9
```

### Именнованные матрицы

Есть 2 функции: `colnames` и `rownames`. Туда передать вектор строк.

### Присоединение матриц

`rbind` и `cbind`. Принимают аргумент `...` многоточие, elepsis - сколько угодно аргументов.

### Функции на матрицах

`apply(<матрица>, <1|2>, <func>)`. Функция `<func>` принимает 1 аргумент!
- Индекс 1 во втором аргументе - применение функции по строкам
- 2 - по столбцам
- 1:2 - к каждому элементу.
```r
> m <-matrix(1:10, ncol=5); m
     [,1] [,2] [,3] [,4] [,5]
[1,]    1    3    5    7    9
[2,]    2    4    6    8   10
> apply(m, 1, function(x) sum(x^2))
[1] 165 220
> apply(m, 2, function(x) sum(x^2))
[1]   5  25  61 113 181
> apply(m, 1:2, function(x) if(x>5) x else 5)
     [,1] [,2] [,3] [,4] [,5]
[1,]    5    5    5    7    9
[2,]    5    5    6    8   10
```

### rowSums, rowMeans, colSums, colMeans
- базовые функции на матрицах

### Diag
Extract or replace the diagonal of a matrix, or construct a diagonal matrix
```r
diag(x = 1, nrow, ncol, names = TRUE)
diag(x) <- value
```

Возвращает разное в зависимости от входа.

| Тип х | Результат вызова diag |
| --- | --- |
| Если x -- положительное число, то | возвращаемое значение -- единичная матрица указанного размера |
| Если x -- вектор хотя бы из двух элементов, то | возвращаемое значение -- диагональная матрица с указанными элементами на диагонали |
| Если x -- матрица, то | возвращаемое значение -- вектор, содержащий диагональные элементы |

```r
> diag(3)
     [,1] [,2] [,3]
[1,]    1    0    0
[2,]    0    1    0
[3,]    0    0    1

> diag(1:3)
     [,1] [,2] [,3]
[1,]    1    0    0
[2,]    0    2    0
[3,]    0    0    3

> matrix(1:6, ncol=3)
     [,1] [,2] [,3]
[1,]    1    3    5
[2,]    2    4    6
> diag(matrix(1:6, ncol=3))
[1] 1 4
```


## Списки

list(...)
Functions to construct, coerce and check for both kinds of **R** lists.

```r
list(...)
pairlist(...)

as.list(x, ...)
## S3 method for class 'environment'
as.list(x, all.names = FALSE, sorted = FALSE, ...)
as.pairlist(x)

is.list(x)
is.pairlist(x)

alist(...)
```

Список обычный и список списков:
```r
> list(1:5)
[[1]]
[1] 1 2 3 4 5

> lapply(c(1:3), function(i) i:(i+2))
[[1]]
[1] 1 2 3

[[2]]
[1] 2 3 4

[[3]]
[1] 3 4 5
```

Может иметь несколько размерностей. См. пример в [[#sapply]].

### Именованные списки
Элементы списка могут иметь имена:
```r
l <- list(number = ind, element = l[[ind]])
l$number # обращение к именованному элементу

> list(number = 1:2, element = 3:4)
$number
[1] 1 2

$element
[1] 3 4

# какая-то жесть
# https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/lapply
list(a = 1:10, beta = exp(-3:3), logic = c(TRUE,FALSE,FALSE,TRUE))
```

См пример [[R - практика#bastille]].
```r
> set.seed(1789)
> bastille <- list(
  "La Chapelle Tower" = rbinom(5, 10, 1/2), 
  "Tresor Tower" = rbinom(8, 12, 1/4), 
  "Comte Tower" = rbinom(14, 3, 1/5) + 1,
  "Baziniere Tower" = rbinom(8, 4, 4/5), 
  "Bertaudiere Tower" = rbinom(4, 8, 2/3),
  "Liberte Tower" = rbinom(1, 100, 0.1), 
  "Puits Tower" = rbinom(5, 5, 0.7),
  "Coin Tower" = rbinom(3, 16, 0.4)
)

> sum_guards <- sapply(bastille, function(item) sum(item))
> sum_guards
La Chapelle Tower      Tresor Tower       Comte Tower   Baziniere Tower 
               28                30                23                24 
Bertaudiere Tower     Liberte Tower       Puits Tower        Coin Tower 
               18                10                20                17 
> names(bastille)
[1] "La Chapelle Tower" "Tresor Tower"      "Comte Tower"       "Baziniere Tower"  
[5] "Bertaudiere Tower" "Liberte Tower"     "Puits Tower"       "Coin Tower"
					   
> sum_guards["Liberte Tower"]
Liberte Tower 
           10
```

### Список2вектор
Метод `unlist`.
```r
> res
        [,1] [,2] [,3] [,4]
element 1    4    5    6   
count   1    1    2    3   
> unlist(res)
[1] 1 1 4 1 5 2 6 3
```

### Многомерные списки
Создаются применением [[#sapply]].

```r
> res
        [,1] [,2] [,3] [,4]
element 1    4    5    6   
count   1    1    2    3   

> res[,1]
$element
[1] 1

$count
[1] 1

> res[1,]
[[1]]
[1] 1

[[2]]
[1] 4

[[3]]
[1] 5

[[4]]
[1] 6
```

### Dimnames
Можно задавать имена:
```r
> res
        [,1] [,2] [,3] [,4]
element 1    4    5    6   
count   1    1    2    3

> colnames(res)
NULL
> rownames(res)
[1] "element" "count"  
> names(res)
NULL

> dim(res)
[1] 2 4
> dimnames(res)
[[1]]
[1] "element" "count"  

[[2]]
NULL
```

### Индексация списков

`l[2]` - подсписок, имеющий индекс 2
`l[[2]]` - второй элемент списка

```r
> l1
[[1]]
[1] -2.3023457 -0.1708760  0.1402782 -1.4974267

[[2]]
[1] -1.0101884 -0.9484756 -0.4939622

[[3]]
[1] -0.17367413 -0.40659878  1.84563626  0.39405411  0.79752850 -1.56666536 -0.08585101

> l1[2]
[[1]]
[1] -1.0101884 -0.9484756 -0.4939622

> l1[[2]]
[1] -1.0101884 -0.9484756 -0.4939622
```

### Функции \[\*\]apply,

```r
apply  # вектор / матрица
lapply # вектор / list
sapply
tapply
mapply
vapply
eapply
```
`apply` returns a vector or array or list of values obtained by applying a function to margins of an array or matrix. См [[#Функции на матрицах]].

`lapply` returns a list of the same length as `X`, each element of which is the result of applying `FUN` to the corresponding element of `X`. См. [[#Списки]].

`sapply` is a user-friendly version and wrapper of `lapply` by default returning a vector, matrix or, if `simplify = "array"`, an array if appropriate, by applying `simplify2array()`. `sapply(x, f, simplify = FALSE, USE.NAMES = FALSE)` is the same as `lapply(x, f)`. См. [[#Списки]].

Функция принимает элемент вектора/списка.

```r
> lapply(3:1, function(item) item^2)
[[1]]
[1] 9

[[2]]
[1] 4

[[3]]
[1] 1

# Итерация с индексами - через замыкание
> items <- 3:1
> table <- lapply(1:length(items), function(idx) items[idx]^2); table
[[1]]
[1] 9

[[2]]
[1] 4

[[3]]
[1] 1	 
```

#### sapply
нихрена не понял, но возвращает список, который похож на матрицу. По `$element` строки достать нельзя - это не датафрейм. См пример [[R - практика#count_elements]].
Попробуй вывести `lapply` и `sapply`.

#### vapply
возвращает вектор. Третий аргумент `FUN.VALUE` - тип данных, возвращаеммых функцией. Типа гарантия, что конечный вектор будет иметь правильные типы данных.
```r
exp2 <- vapply(
  vector(mode="integer", length = N),
  function(arg) sum(rpois(1, a1), rpois(1, a2), rpois(1, a3)),
  numeric(1)
);

# N = 100
[1] 1 5 1 1 1 2 1 0 4 2 1 1 0 1 1 4 3 2 2 1 1 1 0 1 7 2 2 1 0 2 4 2 4 0 1 0 3
[38] 0 2 0 2 3 1 1 4 1 0 1 1 1 2 5 0 3 1 2 0 3 1 3 0 1 1 1 3 6 2 2 1 4 3 0 1 2
[75] 2 4 1 1 4 0 4 2 1 0 0 2 1 3 0 2 3 1 1 2 0 1 7 0 1 3
```



## Датафреймы

```r
m<-50
set.seed(777)
P1<-sample(c(1:10), m, replace=TRUE)
P2<-sample(c(10:100), m, replace=TRUE)
P3<-sample(c(1:130), m, replace=TRUE)
P4<-sample(c(1:100), m, replace=TRUE)
P5<-runif(m, 0,1)
P6<-rexp(m, 0.4)

W<-data.frame(P1,P2,P3,P4,P5,P6)
names<-c()
for (i in c(1:m)){ names<-c(names, paste("W",as.character(i)))}    
rownames(W)<-names
View(W)
```
![[Pasted image 20230214075639.png]]

Сохранение датафрейма в формате .csv
```r
write.csv(W,file="W.csv")
```


### Имена
```r
colnames(Data)<-c("P1","P2","P3","P4","P5","P6","W1", "W2", "S", "R", "T")
for (i in c(1:m)){ names<-c(names, paste("W",as.character(i)))} 
rownames(W)<-names
```
Копирование имен параметров
```r
rownames(WW)<-rownames(NewW)
```


### Индексация Dataframe

`W[i,j]` - элемент i-ой строки, j-ого столбца
`W[i,]` - вектор элементов i-ой строки
`W$P1` - вектор  элементов j-ого столбца под параметра `P1`

```r
for (i in c(1:length(rownames(W)))) {
	... Итерация по строкам
}
```


### Объединение Dataframe
```r
**#create two vectors
a <- c(1, 3, 3, 4, 5)
b <- c(7, 7, 8, 3, 2)

#rbind the two vectors into a matrix
new_matrix <- rbind(a, b)

#view matrix
new_matrix

  [,1] [,2] [,3] [,4] [,5]
a    1    3    3    4    5
b    7    7    8    3    2
```

### Статистика на Dataframe

Можно посмотреть характеристики
```r
> summary(Data)
```
![[Pasted image 20230214083056.png]]


## lpSolve

Решает СЛАУ с ограничениями. Пример из [[АХП (Анализ характеристик производительности)/Лекция 1]]

```r
install.packages("lpSolve")
library(lpSolve)
```

```r
objective.in<-c(-0.3, 0.006, 0.2884, 0.088, 0.132, -0.15)
const.mat<-matrix(c(0.3, -0.045, 0.04, 0.25,0,0,
                  0.7, 0.3, 0.08, 0.12, 0.15, 0.05,
                  0.1, 0.3, 0.8, 0.44, 0.32, -0.2,
                  0.3, -0.03, -0.002, 0.2, -0.32, 0.25,
                  1,0,0,0,0,0,
                  0,1,0,0,0,0,
                  0,0,1,0,0,0,
                  0,0,0,1,0,0,
                  0,0,0,1,0,0,  
                  0,0,0,0,1,0,
                  0,0,0,0,0,1), nrow=11, byrow=TRUE)
const.dir<-c(">=","<=",">=",">=", "<",">",">",">","<=",">","<=")
const.rhs<-c(7, 100,50, 20, 9,  40, 20,13, 150, 0.5,  5)
Res<-lp("max", objective.in, const.mat, const.dir, const.rhs)

Res$solution
0, 40, 931.145320197044, 109.86145320197, 0.5, 5
```


## Регрессия

### Линейная
Функция **lm()** в R используется для подбора моделей линейной регрессии.
Эта функция использует следующий базовый синтаксис:

**lm(формула, данные, …)**
куда:
-   **формула:** формула для линейной модели (например, y ~ x1 + x2)
-   **data:** имя фрейма данных, содержащего данные

Пример: https://www.codecamp.ru/blog/lm-function-in-r/

```r
#fit regression model using hp, drat, and wt as predictors
model <- lm(mpg ~ hp + drat + wt, data = mtcars)

#view model summary
summary(model)
```

Что находится в выоде summary и как это использовать: https://www.codecamp.ru/blog/interpret-regression-output-in-r/

### Подогнанная модель регрессии
Мы можем использовать **функцию abline()** для построения подобранной модели регрессии:

```python
#create scatterplot of raw data
plot(df$x, df$y, col='red', main='Summary of Regression Model', xlab='x', ylab='y')

#add fitted regression line
abline(model)
```


## Формулы

```r
myFormula <- Species ~ Sepal.Length + Sepal.Width + Petal.Length + Petal.Width
```
The thing on the right of `<-` is a [`formula`](https://www.rdocumentation.org/packages/stats/topics/formula) object. It is often used to denote a statistical model, where the thing on the left of the `~` is the response and the things on the right of the `~` are the explanatory variables. So in English you'd say something like _"Species depends on Sepal Length, Sepal Width, Petal Length and Petal Width"_.


## Выражения Expression

### Eval
```r
a<-0.15
b<-5
e<-expression(
  (b + a*z)
)
eval(e, list(a=3, z=1))
> 8
```

### Дисперсия
```r
> P <- expression(15*z*y+1)    
> P
expression(15 * z * y + 1)
> DP <- D(P, 'z'); DP      
15 * y
> typeof(DP)
[1] "language"
```

Оказалось, что функция, считающая производные, не знает оператор \[ \]. Так что если eval может вычислить формулу, коэффициенты которой заданы вектором, то производную этого взять нельзя. Пример:
```r
# x1 <- c(...); x2 <- c(...); x3 <- c(...);
# p1 <- c(...); p2 <- c(...); p3 <- c(...);
e<-expression(
  (p11*z^x11 + p12*z^x12 + p13*z^x13+ p14*z^x14+ p15*z^x15) *
  (p21*z^x21 + p22*z^x22 + p23*z^x23+ p24*z^x24+ p25*z^x25) *
  (p31*z^x31 + p32*z^x32 + p33*z^x33+ p34*z^x34+ p35*z^x35)
)
# eval(e, list(z<-1)) -> ok

# костыль
env<-list(z=1,
  p11=p1[1], p12=p1[2], p13=p1[3], p14=p1[4], p15=p1[5],
  p21=p2[1], p22=p2[2], p23=p2[3], p24=p2[4], p25=p2[5],
  p31=p3[1], p32=p3[2], p33=p3[3], p34=p3[4], p35=p3[5],
  x11=X1[1], x12=X1[2], x13=X1[3], x14=X1[4], x15=X1[5],
  x21=X2[1], x22=X2[2], x23=X2[3], x24=X2[4], x25=X2[5],
  x31=X3[1], x32=X3[2], x33=X3[3], x34=X3[4], x35=X3[5]
)

derrivative_e<-D(e, 'z'); derrivative_e
```
 


## Модели

Используйте модель регрессии, чтобы делать прогнозы

Мы можем использовать функцию **predict()** , чтобы предсказать значение ответа для нового наблюдения:

```r
#fit linear regression model using 'x' as predictor and 'y' as response variable
model <- lm(y ~ x, data=df)

#график
plot(model) 
#view summary of regression model
summary(model)

#define new observation
new <- data.frame(x=c(5))

#use the fitted model to predict the value for the new observation
predict(model, newdata = new)
```


## Пакет Stats

**rpois** - Функция rpois из пакета stats генерит случайные числа, распределенные по закону Пуассона с параметром a=lambda\*tau

Пример из [[Lection_2.ipynb]] 

```r
library(stats)
n<-300000
lambda=1.2
tau<-2
a<-lambda*tau

# Функция rpois из пакета stats генерит случайные числа, распределенные по закону Пуассона с параметром a=lambda*tau

x<-rpois(n,a)
print(length(x[x==0])/n)
print(length(x[x==1])/n)
print(length(x[x>=1])/n)
```