#АХП

## Язык R


## Редактор для работы с R

Можно использовать:
1. [R-studio](https://posit.co) (200 Мб)
2. VSCode (200 Мб)
	- extensions: R
3. R + Jupiter в VSCode (200 Мб + 200 Мб расширений)
	+ extensions: jupiter | R | RServer | ?
	+ R package languageserver
	[Как установить R+Jupiter в VSCode](https://code.visualstudio.com/docs/languages/r). См. Getting started.
4. Google Colab - онлайн


## Для отчетов ЛР

1. Формат jupiter (.ipynb) => любая среда для ведения jupiter:
	- vscode + jupiter extension
	- anaconda
	- Google Colab - онлайн
	- Jupyter.org https://jupyter.org - онлайн
	

## Среда для исчисления алгебраических выражений

1. [maxima](https://sourceforge.net/projects/maxima/files/) (800 Мб)
2. [wolframalpha](https://www.wolframalpha.com) - онлайн








