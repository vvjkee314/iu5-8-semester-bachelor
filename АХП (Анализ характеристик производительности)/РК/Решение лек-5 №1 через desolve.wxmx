PK     �fxV�B�H         mimetypetext/x-wxmathmlPK     �fxV��R  R  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     �fxVA�>�W  �W     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 22.04.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="140">

<cell type="code">
<input>
<editor type="input">
<line>kill(all);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o0)	">(%o0) </lbl><v>done</v>
</mth></output>
</cell>

<cell type="code" question1="&lt;math&gt;&lt;st&gt;Is &lt;/st&gt;&lt;mrow&gt;&lt;p&gt;&lt;mi&gt;lambda&lt;/mi&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mi&gt;mu&lt;/mi&gt;&lt;/p&gt;&lt;/mrow&gt;&lt;h&gt;*&lt;/h&gt;&lt;mrow&gt;&lt;p&gt;&lt;mn&gt;9&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;lambda&lt;/mi&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mi&gt;mu&lt;/mi&gt;&lt;/p&gt;&lt;/mrow&gt;&lt;st&gt; positive, negative or zero?&lt;/st&gt;&lt;/math&gt;" answer1="positive&#10;;">
<input>
<editor type="input">
<line></line>
<line>/* Задаем начальные условия */</line>
<line>atvalue(P0(t),t=0,1)$</line>
<line>atvalue(P1(t),t=0,0)$</line>
<line>atvalue(P2(t),t=0,0)$</line>
<line>/* Решаем систему линейных дифференциальных уравнений */</line>
<line>result:desolve([</line>
<line>        &apos;diff(P0(t),t)=-mu*P1(t)-2*mu*P0(t),</line>
<line>         &apos;diff(P1(t),t)=2*lambda*P0(t)+2*lambda*P2(t)-(lambda+mu)*P1(t),</line>
<line>         &apos;diff(P2(t),t)=lambda*P1(t)-2*mu*P2(t)</line>
<line>    ], [</line>
<line>        P0(t),P1(t),P2(t)</line>
<line>    ]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><r><p><v>lambda</v><v>−</v><v>mu</v></p></r><h>·</h><r><p><n>9</n><h>·</h><v>lambda</v><v>−</v><v>mu</v></p></r><st> positive, negative or zero?</st><editor type="input">
<line>positive</line>
<line>;</line>
</editor>
<lbl altCopy="(%o13)	">(%o13) </lbl><r list="true"><t listdelim="true">[</t><fn><r><fnm>P0</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>3</n><h>·</h><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><r><p><f><r><r><p><f><r><v>mu</v><h>·</h><r><p><v>lambda</v><v>+</v><n>3</n><h>·</h><v>mu</v></p></r></r><r><v>lambda</v><v>−</v><v>mu</v></r></f><v>−</v><f><r><n>2</n><h>·</h><r><p><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></p></r></r><r><v>lambda</v><v>−</v><v>mu</v></r></f></p></r><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><v>−</v><f><r><v>mu</v><h>·</h><fn><r><fnm>cosh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><v>lambda</v><v>−</v><v>mu</v></r></f></p></r><v>+</v><f><r><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><v>mu</v><h>·</h><v>t</v></r></e><h>·</h><v>lambda</v></r><r><v>lambda</v><v>−</v><v>mu</v></r></f><fnm>,</fnm><fn><r><fnm>P1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><f><r><n>4</n><h>·</h><v>lambda</v><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>3</n><h>·</h><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><fnm>,</fnm><fn><r><fnm>P2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>3</n><h>·</h><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><r><p><f><r><r><p><f><r><n>2</n><h>·</h><r><p><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><v>mu</v><h>·</h><v>lambda</v></p></r></r><r><v>lambda</v><v>−</v><v>mu</v></r></f><v>−</v><f><r><v>lambda</v><h>·</h><r><p><v>lambda</v><v>+</v><n>3</n><h>·</h><v>mu</v></p></r></r><r><v>lambda</v><v>−</v><v>mu</v></r></f></p></r><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><v>lambda</v><h>·</h><fn><r><fnm>cosh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><n>9</n><h>·</h><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>10</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><v>lambda</v><v>−</v><v>mu</v></r></f></p></r><v>−</v><f><r><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><v>mu</v><h>·</h><v>t</v></r></e><h>·</h><v>lambda</v></r><r><v>lambda</v><v>−</v><v>mu</v></r></f><t listdelim="true">]</t></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>atvalue(P0(t),t=0,1)$</line>
<line>atvalue(P1(t),t=0,0)$</line>
<line>atvalue(P2(t),t=0,0)$</line>
<line>/* Решаем систему линейных дифференциальных уравнений */</line>
<line>result:desolve([</line>
<line>        &apos;diff(P0(t),t)=-mu*P1(t)-2*mu*P0(t),</line>
<line>         &apos;diff(P1(t),t)=2*lambda*P0(t)+2*lambda*P2(t)-(lambda+mu)*P1(t),</line>
<line>         &apos;diff(P2(t),t)=lambda*P1(t)-2*mu*P2(t)</line>
<line>    ], [</line>
<line>        P0(t),P1(t),P2(t)</line>
<line>    ]);</line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>kill(all);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o0)	">(%o0) </lbl><v>done</v>
</mth></output>
</cell>

<cell type="code" question1="&lt;math&gt;&lt;st&gt;Is &lt;/st&gt;&lt;msup&gt;&lt;mrow&gt;&lt;mi&gt;lambda&lt;/mi&gt;&lt;/mrow&gt;&lt;mn&gt;2&lt;/mn&gt;&lt;/msup&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mn&gt;4&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;lambda&lt;/mi&gt;&lt;mi&gt;+&lt;/mi&gt;&lt;mn&gt;2&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;mu&lt;/mi&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;lambda&lt;/mi&gt;&lt;mi&gt;+&lt;/mi&gt;&lt;mn&gt;8&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;msup&gt;&lt;mrow&gt;&lt;mi&gt;λ&lt;/mi&gt;&lt;/mrow&gt;&lt;mn&gt;2&lt;/mn&gt;&lt;/msup&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mn&gt;8&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;λ&lt;/mi&gt;&lt;mi&gt;+&lt;/mi&gt;&lt;mn&gt;4&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;msup&gt;&lt;mrow&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;/mrow&gt;&lt;mn&gt;2&lt;/mn&gt;&lt;/msup&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mn&gt;4&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;mu&lt;/mi&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;mi&gt;+&lt;/mi&gt;&lt;msup&gt;&lt;mrow&gt;&lt;mi&gt;mu&lt;/mi&gt;&lt;/mrow&gt;&lt;mn&gt;2&lt;/mn&gt;&lt;/msup&gt;&lt;st&gt; positive, negative or zero?&lt;/st&gt;&lt;/math&gt;" answer1="positive;">
<input>
<editor type="input">
<line></line>
<line>atvalue(P0(t),t=0,1)$</line>
<line>atvalue(P1(t),t=0,0)$</line>
<line>atvalue(P2(t),t=0,0)$</line>
<line>/* Решаем систему линейных дифференциальных уравнений */</line>
<line>result:desolve([</line>
<line>        &apos;diff(P0(t),t)=-η*P1(t)-2*η*P0(t),</line>
<line>         &apos;diff(P1(t),t)=2*λ*P0(t)+2*λ*P2(t)-(lambda+mu)*P1(t),</line>
<line>         &apos;diff(P2(t),t)=λ*P1(t)-2*η*P2(t)</line>
<line>    ], [</line>
<line>        P0(t),P1(t),P2(t)</line>
<line>    ]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>lambda</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>η</v><h>·</h><v>lambda</v><v>+</v><n>2</n><h>·</h><v>mu</v><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>positive;</line>
</editor>
<lbl altCopy="(%o4)	">(%o4) </lbl><r list="true"><t listdelim="true">[</t><fn><r><fnm>P0</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>2</n><h>·</h><v>η</v><v>+</v><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><r><p><f><r><r><p><f><r><v>η</v><h>·</h><r><p><v>lambda</v><v>+</v><n>2</n><h>·</h><v>η</v><v>+</v><v>mu</v></p></r></r><r><v>λ</v><v>−</v><v>η</v></r></f><v>−</v><f><r><n>2</n><h>·</h><r><p><v>η</v><h>·</h><v>lambda</v><v>+</v><v>mu</v><h>·</h><v>η</v></p></r></r><r><v>λ</v><v>−</v><v>η</v></r></f></p></r><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><v>−</v><f><r><v>η</v><h>·</h><fn><r><fnm>cosh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><v>λ</v><v>−</v><v>η</v></r></f></p></r><v>+</v><f><r><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><v>t</v><h>·</h><v>η</v></r></e><h>·</h><v>λ</v></r><r><v>λ</v><v>−</v><v>η</v></r></f><fnm>,</fnm><fn><r><fnm>P1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><f><r><n>4</n><h>·</h><v>λ</v><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>2</n><h>·</h><v>η</v><v>+</v><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><fnm>,</fnm><fn><r><fnm>P2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>−</v><f><r><v>t</v><h>·</h><r><p><v>lambda</v><v>+</v><n>2</n><h>·</h><v>η</v><v>+</v><v>mu</v></p></r></r><r><n>2</n></r></f></r></e><h>·</h><r><p><f><r><r><p><f><r><n>2</n><h>·</h><r><p><v>λ</v><h>·</h><v>lambda</v><v>+</v><v>mu</v><h>·</h><v>λ</v></p></r></r><r><v>λ</v><v>−</v><v>η</v></r></f><v>−</v><f><r><v>λ</v><h>·</h><r><p><v>lambda</v><v>+</v><n>2</n><h>·</h><v>η</v><v>+</v><v>mu</v></p></r></r><r><v>λ</v><v>−</v><v>η</v></r></f></p></r><h>·</h><fn><r><fnm>sinh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><v>λ</v><h>·</h><fn><r><fnm>cosh</fnm></r><r><p><f><r><v>t</v><h>·</h><q><e><r><v>lambda</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>·</h><v>mu</v><v>−</v><n>4</n><h>·</h><v>η</v></p></r><h>·</h><v>lambda</v><v>+</v><n>8</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>8</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><n>4</n><h>·</h><e><r><v>η</v></r><r><n>2</n></r></e><v>−</v><n>4</n><h>·</h><v>mu</v><h>·</h><v>η</v><v>+</v><e><r><v>mu</v></r><r><n>2</n></r></e></q></r><r><n>2</n></r></f></p></r></fn></r><r><v>λ</v><v>−</v><v>η</v></r></f></p></r><v>−</v><f><r><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><v>t</v><h>·</h><v>η</v></r></e><h>·</h><v>λ</v></r><r><v>λ</v><v>−</v><v>η</v></r></f><t listdelim="true">]</t></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>kill(all);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o0)	">(%o0) </lbl><v>done</v>
</mth></output>
</cell>

<cell type="code" question1="&lt;math&gt;&lt;st&gt;Is &lt;/st&gt;&lt;mrow&gt;&lt;p&gt;&lt;mi&gt;λ&lt;/mi&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;/p&gt;&lt;/mrow&gt;&lt;h&gt;*&lt;/h&gt;&lt;mrow&gt;&lt;p&gt;&lt;mn&gt;9&lt;/mn&gt;&lt;h&gt;*&lt;/h&gt;&lt;mi&gt;λ&lt;/mi&gt;&lt;mi&gt;-&lt;/mi&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;/p&gt;&lt;/mrow&gt;&lt;st&gt; positive, negative or zero?&lt;/st&gt;&lt;/math&gt;" answer1="positive;&#10;">
<input>
<editor type="input">
<line>atvalue(P0(t),t=0,1)$</line>
<line>atvalue(P1(t),t=0,0)$</line>
<line>atvalue(P2(t),t=0,0)$</line>
<line>/* Решаем систему линейных дифференциальных уравнений */</line>
<line>result:desolve([</line>
<line>        &apos;diff(P0(t),t)=η*P1(t)-2*λ*P0(t),</line>
<line>         &apos;diff(P1(t),t)=2*λ*P0(t)+2*η*P2(t)-(λ+η)*P1(t),</line>
<line>         &apos;diff(P2(t),t)=λ*P1(t)-2*η*P2(t)</line>
<line>    ], [</line>
<line>        P0(t),P1(t),P2(t)</line>
<line>    ]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o12)	">(%o12) </lbl><r list="true"><t listdelim="true">[</t><fn><r><fnm>P0</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><f><r><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><n>2</n><h>·</h><v>λ</v><v>+</v><n>2</n><h>·</h><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><v>λ</v><v>+</v><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><v>η</v></r><r><n>2</n></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><fnm>,</fnm><fn><r><fnm>P1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><v>−</v><f><r><n>2</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><n>2</n><h>·</h><v>λ</v><v>+</v><n>2</n><h>·</h><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><r><p><n>2</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><v>−</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><v>λ</v><v>+</v><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><fnm>,</fnm><fn><r><fnm>P2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><f><r><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><n>2</n><h>·</h><v>λ</v><v>+</v><n>2</n><h>·</h><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><n>2</n><h>·</h><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><v>λ</v><v>+</v><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><v>λ</v></r><r><n>2</n></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><t listdelim="true">]</t></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>%th(1)[1];</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o15)	">(%o15) </lbl><fn><r><fnm>P0</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><f><r><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><n>2</n><h>·</h><v>λ</v><v>+</v><n>2</n><h>·</h><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><v>λ</v><v>+</v><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><v>η</v></r><r><n>2</n></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>rhs(%o15);</line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o41)	">(%o41) </lbl><f><r><e><r><v>λ</v></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><n>2</n><h>·</h><v>λ</v><v>+</v><n>2</n><h>·</h><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><h>·</h><e><r><s>%e</s></r><r><v>−</v><v>t</v><h>·</h><r><p><v>λ</v><v>+</v><v>η</v></p></r></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><v>η</v></r><r><n>2</n></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code" question1="&lt;math&gt;&lt;st&gt;Is &lt;/st&gt;&lt;mi&gt;η&lt;/mi&gt;&lt;st&gt; positive, negative or zero?&lt;/st&gt;&lt;/math&gt;" answer1="pos;" question2="&lt;math&gt;&lt;st&gt;Is &lt;/st&gt;&lt;mi&gt;λ&lt;/mi&gt;&lt;st&gt; positive, negative or zero?&lt;/st&gt;&lt;/math&gt;" answer2="positive;">
<input>
<editor type="input">
<line>limit(%o41, t, inf);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><v>λ</v><st> positive, negative or zero?</st><editor type="input">
<line>positive;</line>
</editor>
<st breakline="true">Is </st><v>η</v><st> positive, negative or zero?</st><editor type="input">
<line>pos;</line>
</editor>
<lbl altCopy="(%o48)	">(%o48) </lbl><f><r><e><r><v>η</v></r><r><n>2</n></r></e></r><r><e><r><v>λ</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>η</v><h>·</h><v>λ</v><v>+</v><e><r><v>η</v></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

</wxMaximaDocument>PK      �fxV�B�H                       mimetypePK      �fxV��R  R  
             5   format.txtPK      �fxVA�>�W  �W               �  content.xmlPK      �   �^    